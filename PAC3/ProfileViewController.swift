//
//  ProfileViewController.swift
//  PR3
//
//  Copyright © 2018 UOC. All rights reserved.
//

import UIKit

class ProfileViewController: UITableViewController, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    var currentProfile: Profile?
    
    // BEGIN-UOC-2
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var surname: UITextField!
    
    @IBOutlet weak var streetAddress: UITextField!
    @IBOutlet weak var city: UITextField!
    
    @IBOutlet weak var occupation: UITextField!
    @IBOutlet weak var company: UITextField!
    @IBOutlet weak var income: UITextField!
    // END-UOC-2
    
    @IBOutlet var profileImage: UIImageView!
    
    override func viewDidLoad() {
        currentProfile = loadProfileData()
        updateOutletsWithProfileDetails()
        
        setTextFieldsDelegates()
        setDismissKeyboardOnViewTap()

        profileImage.image = loadProfileImage()
        makeProfileImageCircleShaped()
    }
    
    // BEGIN-UOC-3
    func saveProfileData(_ profileToSave: Profile?) {
        var success: Bool
        if let profileDataURL = getProfileDataURL(), let newProfile = profileToSave {
            success = NSKeyedArchiver.archiveRootObject(newProfile, toFile: profileDataURL.path)
        } else {
            success = false
        }
        showSavingMessage(success)
    }
    
    func getProfileDataURL() -> URL? {
        let documentDirectories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        if let documentDirectory = documentDirectories.first {
            return documentDirectory.appendingPathComponent("profile_data")
        } else {
            return nil
        }
    }
    
    @IBAction func saveProfile(_ sender: Any) {
        let enteredName: String = getProfileStringDetail(detail: name)
        let enteredSurname: String = getProfileStringDetail(detail: surname)
        
        let enteredStreetAddress: String = getProfileStringDetail(detail: streetAddress)
        let enteredCity: String = getProfileStringDetail(detail: city)
        
        let enteredOccupation: String = getProfileStringDetail(detail: occupation)
        let enteredCompany: String = getProfileStringDetail(detail: company)
        let enteredIncome: Int = Int.init(income.text!) ?? 0
        
        currentProfile = Profile(name: enteredName, surname: enteredSurname, streetAddress: enteredStreetAddress, city: enteredCity, occupation: enteredOccupation, company: enteredCompany, income: enteredIncome)
        saveProfileData(currentProfile)
    }
    
    func getProfileStringDetail(detail: UITextField) -> String{
        if let currentDetailValue = detail.text {
            return currentDetailValue
        } else {
            return ""
        }
    }
    
    func showSavingMessage(_ success: Bool) {
        if success {
            Utils.show(Message: "The profile data has been saved", WithTitle: "Profile saved", InViewController: self)
        } else {
            Utils.show(Message: "An error occured saving the profile data", WithTitle: "Error", InViewController: self)
        }
    }
    // END-UOC-3
    
    // BEGIN-UOC-4
    func loadProfileData() -> Profile {
        if let profileDataURL = getProfileDataURL(), let recoveredProfileData = NSKeyedUnarchiver.unarchiveObject(withFile: profileDataURL.path) as? Profile {
            return recoveredProfileData
        } else {
            return Profile(name: "", surname: "", streetAddress: "", city: "", occupation: "", company: "", income: 0)
        }
    }
    
    func updateOutletsWithProfileDetails() {
        name.text = currentProfile?.name
        surname.text = currentProfile?.surname
        
        streetAddress.text = currentProfile?.streetAddress
        city.text = currentProfile?.city
        
        occupation.text = currentProfile?.occupation
        company.text = currentProfile?.company
        income.text = (String)(currentProfile?.income ?? 0)
    }
    // END-UOC-4
    
    // BEGIN-UOC-5
    func setTextFieldsDelegates() {
        name.delegate = self
        surname.delegate = self
        
        streetAddress.delegate = self
        city.delegate = self
        
        occupation.delegate = self
        company.delegate = self
        income.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case name:
            surname.becomeFirstResponder()
        case surname:
            streetAddress.becomeFirstResponder()
        case streetAddress:
            city.becomeFirstResponder()
        case city:
            occupation.becomeFirstResponder()
        case occupation:
            company.becomeFirstResponder()
        case company:
            income.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func setDismissKeyboardOnViewTap() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == income {
            if string.count > 0 {
                let onlyNumbersCharacterSet = CharacterSet(charactersIn: "0123456789")
                let typedCharacterSet = CharacterSet(charactersIn: string)
                let isAllowed = onlyNumbersCharacterSet.isSuperset(of: typedCharacterSet)
                return isAllowed
            }
        }
        return true
    }
    // END-UOC-5
    
    // BEGIN-UOC-6
    @IBAction func updateProfileImage(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            print("Using camera to take picture")
            imagePicker.sourceType = .camera
        } else {
            print("Choosing a picture from photo library")
            imagePicker.sourceType = .photoLibrary
        }
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let newProfileImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImage.image = newProfileImage
            dismiss(animated: true, completion: nil)
            saveProfileImage(newProfileImage)
        }
    }
    
    func makeProfileImageCircleShaped() {
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
    }
    // END-UOC-6
    
    // BEGIN-UOC-7
    func loadProfileImage() -> UIImage? {
        if let profileImageURL = getProfileImageURL(), let recoveredImage = UIImage(contentsOfFile: profileImageURL.path) {
            return recoveredImage
        } else {
            return UIImage(named: "EmptyProfile.png")
        }
    }
    
    func saveProfileImage(_ image: UIImage) {
        var success: Bool = false
        if let profileImageURL = getProfileImageURL(), let imageData = image.jpegData(compressionQuality: 0.8) {
            do {
                try imageData.write(to: profileImageURL, options: [.atomic])
                success = true
            } catch {
                success = false
            }
        }
        showLoadingMessage(success)
    }
    
    func getProfileImageURL() -> URL? {
        let documentDirectories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        if let documentDirectory = documentDirectories.first {
            return documentDirectory.appendingPathComponent("profile_image.jpg")
        } else {
            return nil
        }
    }
    
    func showLoadingMessage(_ success: Bool) {
        if success {
            Utils.show(Message: "The profile image has been saved", WithTitle: "Image saved", InViewController: self)
        } else {
            Utils.show(Message: "An error occured saving the profile image", WithTitle: "Error", InViewController: self)
        }
    }
    // END-UOC-7
}
